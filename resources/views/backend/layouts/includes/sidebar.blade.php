<div class="page-sidebar navbar-collapse collapse">
    <ul class="page-sidebar-menu" data-auto-scroll="true" data-slide-speed="200">
        <li class="sidebar-search-wrapper">
            <form class="sidebar-search" action="#" method="POST">
                <a href="javascript:;" class="remove">
                    <i class="fa fa-times"></i>
                </a>
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="{{ trans('strings.search_placeholder') }}">
						<span class="input-group-btn">
						<a href="javascript:;" class="btn submit"><i class="fa fa-search"></i></a>
						</span>
                </div>
            </form>
        </li>
        <li class="start heading">
            <h3 class="uppercase">{{ trans('menus.general') }}</h3>
        </li>
        <li class="{{ Active::pattern('admin/dashboard') }}">
            <a href="{{url('admin/dashboard')}}">
                <i class="fa fa-home"></i>
				<span class="title">{{ trans('menus.dashboard') }}</span>
            </a>
        </li>
        @permission('view-access-management')
        <li class="heading">
            <h3 class="uppercase">{{ trans('menus.access_management') }}</h3>
        </li>
        <li class="{{ Active::pattern('admin/access/user*','active open')}}">
            <a href="javascript:;">
                <i class="fa fa-users"></i>
                <span class="title">{{ trans('menus.header_buttons.users.button') }}</span>
				<span class="arrow {{ Active::pattern('admin/access/user*','open') }}">
				</span>
            </a>
            <ul class="sub-menu">
                <li class="{{ Active::pattern('admin/access/users') }}">
                    <a href="{{route('admin.access.users.index')}}">
                        <i class="fa fa-users"></i>
                        {{ trans('menus.header_buttons.users.all')}}
                    </a>
                </li>
                @permission('create-users')
                <li class="{{ Active::pattern('admin/access/users/create') }}">
                    <a href="{{route('admin.access.users.create')}}">
                        <i class="fa fa-user"></i>
                        {{ trans('menus.create_user') }}
                    </a>
                </li>
                @endauth
                <li class="{{ Active::pattern('admin/access/users/deactivated') }}">
                    <a href="{{route('admin.access.users.deactivated')}}">
                        <i class="fa fa-unlock"></i>
                        {{ trans('menus.deactivated_users') }}
                    </a>
                </li>
                <li class="{{ Active::pattern('admin/access/users/banned') }}">
                    <a href="{{route('admin.access.users.banned')}}">
                        <i class="fa fa-lock"></i>
                        {{ trans('menus.banned_users') }}
                    </a>
                </li>
                <li class="{{ Active::pattern('admin/access/users/deleted') }}">
                    <a href="{{route('admin.access.users.deleted')}}">
                        <i class="fa fa-trash"></i>
                        {{ trans('menus.deleted_users') }}
                    </a>
                </li>
            </ul>
        </li>
        <li class="{{ Active::pattern('admin/access/roles_m*','active open')}}">
            <a href="javascript:;">
                <i class="fa fa-user-md"></i>
                <span class="title">{{ trans('menus.header_buttons.roles.button') }}</span>
				<span class="arrow {{ Active::pattern('admin/access/roles_m*','open') }}">
				</span>
            </a>
            <ul class="sub-menu">
                <li class="{{ Active::pattern('admin/access/roles_m') }}">
                    <a href="{{route('admin.access.roles_m.index')}}">
                        <i class="fa fa-sliders"></i>
                        {{ trans('menus.header_buttons.roles.all')}}
                    </a>
                </li>
                @permission('create-roles')
                <li class="{{ Active::pattern('admin/access/roles_m/create') }}">
                    <a href="{{route('admin.access.roles_m.create')}}">
                        <i class="fa fa-plus"></i>
                        {{ trans('menus.create_role') }}
                    </a>
                </li>
                @endauth
            </ul>
        </li>
        <li class="{{ Active::pattern('admin/access/roles/permission*','active open')}}">
            <a href="javascript:;">
                <i class="fa fa-tasks"></i>
                <span class="title">{{ trans('menus.header_buttons.permissions.button') }}</span>
				<span class="arrow {{ Active::pattern('admin/access/roles/permission*','open') }}">
				</span>
            </a>
            <ul class="sub-menu">
                <li class="{{ Active::pattern('admin/access/roles/permissions') }}">
                    <a href="{{route('admin.access.roles.permissions.index')}}">
                        <i class="fa fa-sliders"></i>
                        {{ trans('menus.header_buttons.permissions.all')}}
                    </a>
                </li>
                @permission('create-permissions')
                <li class="{{ Active::pattern('admin/access/roles/permissions/create') }}">
                    <a href="{{route('admin.access.roles.permissions.create')}}">
                        <i class="fa fa-plus"></i>
                        {{ trans('menus.create_permission') }}
                    </a>
                </li>
                @endauth
                <li class="{{ Active::pattern('admin/access/roles/permission-group') }}">
                    <a href="{{route('admin.access.roles.permission-group.index')}}">
                        <i class="fa fa-sliders"></i>
                        {{ trans('menus.header_buttons.permissions.groups.all')}}
                    </a>
                </li>
                @permission('create-permission-groups')
                <li class="{{ Active::pattern('admin/access/roles/permission-group/create') }}">
                    <a href="{{route('admin.access.roles.permission-group.create')}}">
                        <i class="fa fa-plus"></i>
                        {{ trans('menus.create_permission_group') }}
                    </a>
                </li>
                @endauth
            </ul>
        </li>
        @endauth
        <li class="last {{ Active::pattern('admin/log-viewer*','active open') }}">
            <a href="javascript:;">
                <i class="fa fa-send-o"></i>
				<span class="title">{{ trans('menus.log-viewer.main') }} </span>
				<span class="arrow {{ Active::pattern('admin/log-viewer*','open') }}">
				</span>
            </a>
            <ul class="sub-menu">
                <li class="{{ Active::pattern('admin/log-viewer') }}">
                    <a href="{!! url('admin/log-viewer') !!}">
                        <i class="fa fa-warning"></i>
                        {{ trans('menus.log-viewer.dashboard') }}
                    </a>
                </li>
                <li class="{{ Active::pattern('admin/log-viewer/logs') }}">
                    <a href="{!! url('admin/log-viewer/logs') !!}">
                        <i class="fa fa-file"></i>
                        {{ trans('menus.log-viewer.logs') }}
                    </a>
                </li>
            </ul>
        </li>
    </ul>
    <!-- END SIDEBAR MENU1 -->
</div>