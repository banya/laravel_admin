<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title>@yield('title', app_name())</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta name="description" content="@yield('meta_description', '晚黎后台管理系统')">
    <meta name="author" content="@yield('author', 'wanli')">
    <meta name="_token" content="{{ csrf_token() }}" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
    <link href="{{asset('backend/vendor/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('backend/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('backend/vendor/uniform/css/uniform.default.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('backend/vendor/bootstrap-switch/css/bootstrap-switch.min.css')}}" rel="stylesheet" type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->
    @yield('css')
    <!-- BEGIN THEME STYLES -->
    <link href="{{asset('backend/css/components-md.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('backend/css/plugins-md.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('backend/css/layout.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('backend/css/darkblue.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('backend/css/custom.css')}}" rel="stylesheet" type="text/css"/>
    <!-- END THEME STYLES -->
    <link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-md">
<!-- BEGIN HEADER -->
@include('backend.layouts.includes.header')
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN SIDEBAR -->
    @include('backend.layouts.includes.sidebar')
    <!-- END SIDEBAR -->
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content page-content-body">
            <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <!-- BEGIN PAGE HEADER-->
            <h3 class="page-title">
                @yield('page-title')
            </h3>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    @yield('breadcrumbs')
                </ul>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row">
                <div class="col-md-12">
                    @yield('content')
                </div>
            </div>
            <!-- END PAGE CONTENT-->
        </div>
    </div>
    <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="page-footer">
    <div class="page-footer-inner">
        2015 &copy; 晚黎
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>
<!-- END FOOTER -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="{{asset('backend/vendor/respond.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/vendor/excanvas.min.js')}}" type="text/javascript"></script>
<![endif]-->
<script src="{{asset('backend/vendor/jquery.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/vendor/jquery-migrate.min.js')}}" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="{{asset('backend/vendor/jquery-ui/jquery-ui.min.js/')}}" type="text/javascript"></script>
<script src="{{asset('backend/vendor/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/vendor/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/vendor/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/vendor/jquery.blockui.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/vendor/jquery.cokie.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/vendor/uniform/jquery.uniform.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/vendor/bootstrap-switch/js/bootstrap-switch.min.js')}}" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<script src="{{asset('backend/js/metronic.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/js/layout.js')}}" type="text/javascript"></script>
<script>
    jQuery(document).ready(function() {
        Metronic.init(); // init metronic core components
        Layout.init(); // init current layout
        $('form[name=delete_item]').submit(function(){
            return confirm("Are you sure you want to delete this item?");
        });
    });
</script>
@yield('js')
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>