@extends('backend.layouts.main')

@section('page-title')
    <h1>Laravel 5 admin <small>{{ trans('strings.backend.dashboard_title') }}</small></h1>
@endsection

@section('breadcrumbs')
    <li>
        <i class="fa fa-home"></i>
        <a href="{!!route('backend.dashboard')!!}">{{ trans('menus.dashboard') }}</a>
        <i class="fa fa-angle-right"></i>
    </li>
    <li class="active">{{ trans('strings.here') }}</li>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('strings.backend.WELCOME') }} {!! auth()->user()->name !!}!</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
            @include('backend.lang.' . app()->getLocale() . '.welcome')
        </div><!-- /.box-body -->
    </div><!--box box-success-->
@endsection