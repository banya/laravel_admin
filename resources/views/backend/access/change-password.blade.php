@extends ('backend.layouts.main')

@section ('title', 'User Management | Change User Password')

@section('page-title')
    <h1>
        User Management
        <small>Change Password</small>
    </h1>
@endsection

@section ('breadcrumbs')
    <li><a href="{!!route('backend.dashboard')!!}"><i class="fa fa-home"></i> Dashboard</a><i class="fa fa-angle-right"></i></li>
    <li>{!! link_to_route('admin.access.users.index', 'User Management') !!}<i class="fa fa-angle-right"></i></li>
    <li>{!! link_to_route('admin.access.users.edit', "Edit ".$user->name, $user->id) !!}<i class="fa fa-angle-right"></i></li>
    <li class="active">Change Password</li>
@stop

@section('content')
    @if (count($errors) > 0)
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
            <h3><strong> Whoops! </strong>There were some problems with your input. </h3>
            <ul class="list-group">
                @foreach ($errors->all() as $error)
                    <li class="list-group-item list-group-item-danger">{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="col-md-12">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-green-haze">
                    <i class="icon-settings font-green-haze"></i>
                    <span class="caption-subject bold uppercase"> {!! trans('labels.change_password_box_title') !!}</span>
                </div>
            </div>
            <div class="portlet-body form">
                <form role="form" action="{{route('admin.access.user.change-password', $user->id)}}" method="post" class="form-horizontal">
                    <input name="_token" type="hidden" value="{{csrf_token()}}">
                    <div class="form-body">
                        <div class="form-group form-md-line-input">
                            <label class="col-md-2 control-label" for="form_control_1">{{trans('validation.attributes.password')}}</label>
                            <div class="col-md-10">
                                <input type="password" class="form-control" id="form_control_1" name="password">
                                <div class="form-control-focus">
                                </div>
                            </div>
                        </div>

                        <div class="form-group form-md-line-input">
                            <label class="col-md-2 control-label" for="form_control_1">{{trans('validation.attributes.password_confirmation')}}</label>
                            <div class="col-md-10">
                                <input type="password" class="form-control" id="form_control_1" name="password_confirmation">
                                <div class="form-control-focus">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-2 col-md-10">
                                <a href="{{route('admin.access.users.index')}}" class="btn default">{{ trans('strings.cancel_button') }}</a>
                                <input type="submit" class="btn blue" value="{{ trans('strings.save_button') }}" />
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop