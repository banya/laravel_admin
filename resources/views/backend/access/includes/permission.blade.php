<div class="portlet light">
    <div class="portlet-body">
        <div class="table-scrollable">
            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>{!! trans('menus.user_management') !!}</th>
                    <th>{{ trans('crud.users.other_permissions') }}</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($users->permissions as $user)
                    <tr>
                        <td>{!! $user->name !!}</td>
                        <td>{!! $user->display_name !!}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>