@extends ('backend.layouts.main')

@section ('title', trans('menus.role_management'))

@section('page-title')
    <h1>
        {{ trans('menus.user_management') }}
        <small>{{ trans('menus.role_management') }}</small>
    </h1>
@endsection

@section ('breadcrumbs')
    <li><a href="{!!route('backend.dashboard')!!}"><i class="fa fa-dashboard"></i> {{ trans('menus.dashboard') }}</a><i class="fa fa-angle-right"></i></li>
    <li>{!! link_to_route('admin.access.users.index', trans('menus.user_management')) !!}<i class="fa fa-angle-right"></i></li>
    <li class="active">{!! trans('menus.role_management') !!}</li>
@stop

@section('content')

    @if(Session::get('flash_success'))
        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
            @if(is_array(json_decode(Session::get('flash_success'),true)))
                {!! implode('', Session::get('flash_success')->all(':message<br/>')) !!}
            @else
                {!! Session::get('flash_success') !!}
            @endif
        </div>
    @endif
    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-th-list"></i>{!! trans('menus.role_management') !!}
            </div>
            <div class="tools">
                <a href="javascript:;" class="collapse" data-original-title="" title=""></a>
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-scrollable">
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>{{ trans('crud.roles.role') }}</th>
                        <th>{{ trans('crud.roles.permissions') }}</th>
                        <th>{{ trans('crud.roles.number_of_users') }}</th>
                        <th>{{ trans('crud.roles.sort') }}</th>
                        <th>{{ trans('crud.actions') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($roles as $role)
                        <tr>
                            <td>{!! $role->name !!}</td>
                            <td>
                                @if ($role->all)
                                    <span class="label label-success">All</span>
                                @else
                                    @if (count($role->permissions) > 0)
                                            <?php $str = ''; ?>
                                            @foreach ($role->permissions as $permission)
                                                <?php
                                                 $str .= $permission->display_name.'<br/>';
                                                 ?>
                                            @endforeach
                                        <button type="button" class="btn btn-xs btn-info tooltips" style="color:black;text-decoration:none;" data-html="true" data-container="body" data-original-title="{{ $str }}"  data-placement="right"><i class="fa fa-search"></i></button>
                                    @else
                                        <span class="label label-danger">None</span>
                                    @endif
                                @endif
                            </td>
                            <td>{!! $role->users()->count() !!}</td>
                            <td>{!! $role->sort !!}</td>
                            <td>{!! $role->action_buttons !!}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
        <div class="portlet-title">
            <div class="pull-left">
                <h5 class="block">{!! $roles->total() !!} {{ trans('crud.roles.total') }}</h5>
            </div>

            <div class="pull-right">
                {!! $roles->render() !!}
            </div>
        </div>

    </div>
    <div class="clearfix"></div>
    <div id="myModal2" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Other Permissions</h4>
                </div>
                <div class="modal-body">
                    <p class="per_content">
                        Permissions loading
                    </p>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn green">OK</button>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')

@stop