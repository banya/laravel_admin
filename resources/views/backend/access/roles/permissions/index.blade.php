@inject('roles', 'App\Repositories\Backend\Role\RoleRepositoryContract')

@extends ('backend.layouts.main')

@section ('title', trans('menus.permission_management'))

@section('page-title')
    <h1>
        {{ trans('menus.user_management') }}
        <small>{{ trans('menus.permission_management') }}</small>
    </h1>
@endsection

@section ('breadcrumbs')
    <li><a href="{!!route('backend.dashboard')!!}"><i class="fa fa-dashboard"></i> {{ trans('menus.dashboard') }}</a><i class="fa fa-angle-right"></i></li>
    <li>{!! link_to_route('admin.access.users.index', trans('menus.user_management')) !!}<i class="fa fa-angle-right"></i></li>
    <li class="active">{!! trans('menus.permission_management') !!}</li>
@stop

@section('content')
    @if(Session::get('flash_success'))
        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
            @if(is_array(json_decode(Session::get('flash_success'),true)))
                {!! implode('', Session::get('flash_success')->all(':message<br/>')) !!}
            @else
                {!! Session::get('flash_success') !!}
            @endif
        </div>
    @endif
    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-sliders"></i>{{ trans('menus.permission_management') }}
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-scrollable">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>{{ trans('crud.permissions.permission') }}</th>
                            <th>{{ trans('crud.permissions.name') }}</th>
                            <th>{{ trans('crud.permissions.dependencies') }}</th>
                            <th>{{ trans('crud.permissions.users') }}</th>
                            <th>{{ trans('crud.permissions.roles') }}</th>
                            <th>{{ trans('crud.permissions.group') }}</th>
                            <th>{{ trans('crud.permissions.group-sort') }}</th>
                            <th>{{ trans('crud.permissions.system') }}</th>
                            <th>{{ trans('crud.actions') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($permissions as $permission)
                        <tr>
                            <td>{!! $permission->name !!}</td>
                            <td>{!! $permission->display_name !!}</td>
                            <td>
                                @if (count($permission->dependencies))
                                    <?php $str = ''; ?>
                                    @foreach($permission->dependencies as $dependency)
                                        <?php
                                        $str .= $dependency->permission->display_name.'<br/>';
                                        ?>
                                    @endforeach
                                    <button type="button" class="btn btn-xs btn-info tooltips" style="color:black;text-decoration:none;" data-html="true" data-container="body" data-original-title="{{ $str }}"  data-placement="right"><i class="fa fa-search"></i>View</button>
                                @else
                                    <span class="label label-success">None</span>
                                @endif
                            </td>
                            <td>
                                @if (count($permission->users))
                                    <?php $my_users = ''; ?>
                                    @foreach($permission->users as $user)
                                        <?php $my_users .= $user->name.'<br/>' ?>
                                    @endforeach
                                        <button type="button" class="btn btn-xs btn-success tooltips" style="color:black;text-decoration:none;" data-html="true" data-container="body" data-original-title="{{ $my_users }}"  data-placement="right"><i class="fa fa-search"></i>View</button>
                                @else
                                    <span class="label label-danger">None</span>
                                @endif
                            </td>
                            <td>
                                <?php $my_role = $roles->findOrThrowException(1)->name.'<br/>'; ?>
                                @if (count($permission->roles))
                                    @foreach($permission->roles as $role)
                                        <?php
                                            $my_role .= $role->name.'<br/>';
                                        ?>
                                    @endforeach
                                @endif
                                    <button type="button" class="btn btn-xs btn-warning tooltips" style="color:black;text-decoration:none;" data-html="true" data-container="body" data-original-title="{{ $my_role }}"  data-placement="right"><i class="fa fa-search"></i>View</button>
                            </td>
                            <td>
                                @if ($permission->group)
                                    {!! $permission->group->name !!}
                                @else
                                    <span class="label label-danger">None</span>
                                @endif
                            </td>
                            <td>{!! $permission->sort !!}</td>
                            <td>{!! $permission->system_label !!}</td>
                            <td>{!! $permission->action_buttons !!}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="portlet-title">
            <div class="pull-left">
                <h5 class="block">{{ $permissions->total() }} {{ trans('crud.permissions.total') }}</h5>
            </div>

            <div class="pull-right">
                {!! $permissions->render() !!}
            </div>
        </div>
    </div>
@stop