@extends ('backend.layouts.main')

@section ('title', trans('menus.permission_management') . ' | ' . trans('menus.create_permission'))

@section('page-title')
    <h1>
        {{ trans('menus.user_management') }}
        <small>{{ trans('menus.create_permission') }}</small>
    </h1>
@endsection

@section ('breadcrumbs')
    <li><a href="{!!route('backend.dashboard')!!}"><i class="fa fa-dashboard"></i> {{ trans('menus.dashboard') }}</a><i class="fa fa-angle-right"></i></li>
    <li>{!! link_to_route('admin.access.users.index', trans('menus.user_management')) !!}<i class="fa fa-angle-right"></i></li>
    <li>{!! link_to_route('admin.access.roles.permissions.index', trans('menus.permission_management')) !!}<i class="fa fa-angle-right"></i></li>
    <li class="active">{!! trans('menus.create_permission') !!}</li>
@stop

@section('content')

    @if (count($errors) > 0)
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
            <h3><strong> Whoops! </strong>There were some problems with your input. </h3>
            <ul class="list-group">
                @foreach ($errors->all() as $error)
                    <li class="list-group-item list-group-item-danger">{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="portlet box purple">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-sliders"></i>{{ trans('menus.create_permission') }}
            </div>
        </div>
        <div class="portlet-body">
            <form class="form-horizontal" action="{{route('admin.access.roles.permissions.store')}}" role="form" method="post">
                <input name="_token" type="hidden" value="{{csrf_token()}}">
                <div class="tabbable-line">
                    <ul class="nav nav-tabs ">
                        <li class="active">
                            <a href="#tab_15_1" data-toggle="tab" aria-expanded="true">
                                General </a>
                        </li>
                        <li class="">
                            <a href="#tab_15_2" data-toggle="tab" aria-expanded="false">
                                Dependencies</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_15_1">
                            <div class="form-group form-md-line-input">
                                <label class="col-md-2 control-label" for="form_control_1">{{trans('validation.attributes.permission_name')}}</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" id="form_control_1" name="name" placeholder="{{trans('validation.attributes.permission_name')}}">
                                    <div class="form-control-focus">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group form-md-line-input">
                                <label class="col-md-2 control-label" for="form_control_1">{{trans('validation.attributes.display_name')}}</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" id="form_control_1" name="display_name" placeholder="{{trans('validation.attributes.display_name')}}">
                                    <div class="form-control-focus">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group form-md-line-input">
                                <label class="col-md-2 control-label" for="form_control_1">{{trans('validation.attributes.group')}}</label>
                                <div class="col-md-10">
                                    <select class="form-control" name="group" id="form_control_1">
                                        <option value="">None</option>
                                        @foreach ($groups as $group)
                                            <option value="{!! $group->id !!}">{!! $group->name !!}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group form-md-line-input">
                                <label class="col-md-2 control-label" for="form_control_1">{{trans('validation.attributes.group-sort')}}</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" id="form_control_1" name="sort" placeholder="{{trans('validation.attributes.group-sort')}}">
                                    <div class="form-control-focus">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group form-md-line-input">
                                <label class="col-md-2 control-label" for="form_control_1">{{trans('validation.attributes.associated_roles')}}</label>
                                <div class="col-md-10">
                                    @if (count($roles) > 0)
                                        <div class="md-checkbox-inline">
                                            @foreach($roles as $role)
                                                <div class="md-checkbox has-success">
                                                    <input type="checkbox" {{$role->id == 1 ? 'disabled checked' : ''}} id="role-{{$role->id}}" value="{{$role->id}}" name="permission_roles[]" class="md-check">
                                                    <label for="role-{{$role->id}}">
                                                        <span class="inc"></span>
                                                        <span class="check"></span>
                                                        <span class="box"></span>
                                                        {!! $role->name !!} </label>
                                                </div>
                                            @endforeach
                                        </div>
                                    @else
                                        No Roles to set
                                    @endif
                                </div>
                            </div>
                            <div class="form-group form-md-line-input">
                                <label class="col-md-2 control-label" for="form_control_1">{{trans('validation.attributes.system_permission')}}</label>
                                <div class="col-md-10">
                                    <div class="md-checkbox-inline">
                                        <div class="md-checkbox has-success">
                                            <input type="checkbox"  id="system" name="system" class="md-check">
                                            <label for="system">
                                                <span class="inc"></span>
                                                <span class="check"></span>
                                                <span class="box"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_15_2">
                            <div class="alert alert-info">
                                <i class="fa fa-info-circle"></i> This section is where you specify that this permission depends on the user having one or more other permissions.<br/><br/>
                                For example: This permission may be <strong>create-user</strong>, but if the user doesn't also have <strong>view-backend</strong> and <strong>view-access-management</strong> permissions they will never be able to get to the <strong>Create User</strong> screen.
                            </div>
                            <div class="form-group form-md-line-input">
                                <label class="col-md-2 control-label" for="form_control_1">{{ trans('validation.attributes.dependencies') }}</label>
                                <div class="col-md-10">
                                    @if (count($permissions))
                                        @foreach (array_chunk($permissions->toArray(), 10) as $perm)
                                            <div class="col-lg-4">
                                                <ul style="margin:0;padding:0;list-style:none;">
                                                    @foreach ($perm as $p)
                                                        <?php
                                                        //Since we are using array format to nicely display the permissions in rows
                                                        //we will just manually create an array of dependencies since we do not have
                                                        //access to the relationship to use the lists() function of eloquent
                                                        //but the relationships are eager loaded in array format now
                                                        $dependencies = [];
                                                        $dependency_list = [];
                                                        if (count($p['dependencies'])) {
                                                            foreach ($p['dependencies'] as $dependency) {
                                                                array_push($dependencies, $dependency['dependency_id']);
                                                                array_push($dependency_list, $dependency['permission']['display_name']);
                                                            }
                                                        }
                                                        $dependencies = json_encode($dependencies);
                                                        $dependency_list = implode(", ", $dependency_list);
                                                        ?>

                                                        <li><input type="checkbox" value="{{$p['id']}}" name="dependencies[]" data-dependencies="{!! $dependencies !!}" id="permission-{{$p['id']}}" /> <label for="permission-{{$p['id']}}" />

                                                            @if ($p['dependencies'])
                                                                <a style="color:black;text-decoration:none;" class="tooltips" data-container="body" data-original-title="<strong>Dependencies:</strong> {!! $dependency_list !!}" data-html="true" data-placement="right">{!! $p['display_name'] !!} <small><strong>(D)</strong></small></a>
                                                                @else
                                                                {!! $p['display_name'] !!}
                                                                @endif

                                                                </label></li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endforeach
                                    @else
                                        No permission to choose from.
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-2 col-md-10">
                            <a href="{{route('admin.access.roles.permissions.index')}}" class="btn default">{{ trans('strings.cancel_button') }}</a>
                            <input type="submit" class="btn blue" value="{{ trans('strings.save_button') }}" />
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop