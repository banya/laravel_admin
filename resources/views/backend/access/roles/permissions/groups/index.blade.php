@inject('roles', 'App\Repositories\Backend\Role\RoleRepositoryContract')

@extends ('backend.layouts.main')

@section ('title', trans('menus.permission_management'))

@section('page-title')
    <h1>
        {{ trans('menus.user_management') }}
        <small>{{ trans('menus.permission_management') }}</small>
    </h1>
@endsection

@section('css')
    <link rel="stylesheet" href="{{asset('backend/vendor/nestable/jquery.nestable.css')}}"/>
    <link rel="stylesheet" href="{{asset('backend/vendor/bootstrap-toastr/toastr.min.css')}}"/>
@stop

@section ('breadcrumbs')
    <li><a href="{!!route('backend.dashboard')!!}"><i class="fa fa-dashboard"></i> {{ trans('menus.dashboard') }}</a><i class="fa fa-angle-right"></i></li>
    <li>{!! link_to_route('admin.access.users.index', trans('menus.user_management')) !!}<i class="fa fa-angle-right"></i></li>
    <li class="active">{!! trans('menus.permission_management') !!}</li>
@stop

@section('content')
    @if(Session::get('flash_success'))
        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
            @if(is_array(json_decode(Session::get('flash_success'),true)))
                {!! implode('', Session::get('flash_success')->all(':message<br/>')) !!}
            @else
                {!! Session::get('flash_success') !!}
            @endif
        </div>
    @endif
    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-sliders"></i>{{ trans('menus.permission_management') }}
            </div>
        </div>
        <div class="portlet-body">
            <div class="row">
                <div class="col-lg-6">
                    <div class="alert alert-info">
                        <i class="fa fa-info-circle"></i> This section allows you to organize your permissions into groups to stay organized. Regardless of the group, the permissions are still individually assigned to each role.
                    </div><!--alert info-->

                    <div class="dd permission-hierarchy">
                        <ol class="dd-list">
                            @foreach ($groups as $group)
                                <li class="dd-item" data-id="{!! $group->id !!}">
                                    <div class="dd-handle">{!! $group->name !!} <span class="pull-right">{!! $group->permissions->count() !!} permissions</span></div>

                                    @if ($group->children->count())
                                        <ol class="dd-list">
                                            @foreach($group->children as $child)
                                                <li class="dd-item" data-id="{!! $child->id !!}">
                                                    <div class="dd-handle">{!! $child->name !!} <span class="pull-right">{!! $child->permissions->count() !!} permissions</span></div>
                                                </li>
                                            @endforeach
                                        </ol>
                                </li>
                                @else
                                </li>
                                @endif
                            @endforeach
                        </ol>
                    </div><!--master-list-->
                </div><!--col-lg-4-->

                <div class="col-lg-6">
                    <div class="alert alert-info">
                        <i class="fa fa-info-circle"></i> If you performed operations in the hierarchy section without refreshing this page, you will need to refresh to reflect the changes here.
                    </div><!--alert info-->

                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>{{ trans('crud.permissions.groups.name') }}</th>
                            <th>{{ trans('crud.actions') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($groups as $group)
                            <tr>
                                <td>
                                    {!! $group->name !!}

                                    @if ($group->permissions->count())
                                        <div style="padding-left:40px;font-size:.8em">
                                            @foreach ($group->permissions as $permission)
                                                {!! $permission->display_name !!}<br/>
                                            @endforeach
                                        </div>
                                    @endif
                                </td>
                                <td>{!! $group->action_buttons !!}</td>
                            </tr>

                            @if ($group->children->count())
                                @foreach ($group->children as $child)
                                    <tr>
                                        <td style="padding-left:40px">
                                            <em>{!! $child->name !!}</em>

                                            @if ($child->permissions->count())
                                                <div style="padding-left:40px;font-size:.8em">
                                                    @foreach ($child->permissions as $permission)
                                                        {!! $permission->display_name !!}<br/>
                                                    @endforeach
                                                </div>
                                            @endif
                                        </td>
                                        <td>{!! $child->action_buttons !!}</td>
                                    </tr>
                                @endforeach
                            @endif
                        @endforeach
                        </tbody>
                    </table>
                </div><!--col-lg-8-->
            </div><!--row-->
        </div>
    </div>
@stop

@section('js')
    <script src="{{asset('backend/vendor/nestable/jquery.nestable.js')}}"></script>
    <script src="{{asset('backend/vendor/bootstrap-toastr/toastr.min.js')}}"></script>
    <script>
        $(function() {
            var hierarchy = $('.permission-hierarchy');
            hierarchy.nestable({maxDepth:2});

            hierarchy.on('change', function() {
                @permission('sort-permission-groups')
                $.ajax({
                    url : "{!! route('admin.access.roles.groups.update-sort') !!}",
                    beforeSend: function(xhr) {
                        xhr.setRequestHeader('X-CSRF-Token', $('meta[name="_token"]').attr('content'))
                    },
                    type: "post",
                    data : {data:hierarchy.nestable('serialize')},
                    success: function(data) {
                        if (data.status == "OK") {
                            toastr.success("Hierarchy successfully saved.");
                        }else{
                            toastr.error("An unknown error occurred.");
                        }

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        toastr.error("An unknown error occurred: " + errorThrown);
                    }
                });
                @else
                    toastr.error("You do not have permission to do that.");
                @endauth
            });
        });
    </script>
@stop