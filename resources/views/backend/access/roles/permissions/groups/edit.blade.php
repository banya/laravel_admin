@extends ('backend.layouts.main')

@section ('title', trans('menus.permission_management') . ' | ' . trans('menus.edit_permission_group'))

@section('page-title')
    <h1>
        {{ trans('menus.permission_management') }}
        <small>{{ trans('menus.edit_permission_group') }}</small>
    </h1>
@endsection

@section ('breadcrumbs')
    <li><a href="{!!route('backend.dashboard')!!}"><i class="fa fa-dashboard"></i> {{ trans('menus.dashboard') }}</a><i class="fa fa-angle-right"></i></li>
    <li>{!! link_to_route('admin.access.users.index', trans('menus.user_management')) !!}<i class="fa fa-angle-right"></i></li>
    <li>{!! link_to_route('admin.access.roles.permissions.index', trans('menus.permission_management')) !!}<i class="fa fa-angle-right"></i></li>
    <li class="active">{!! trans('menus.edit_permission_group') !!}</li>
@stop

@section('content')

    @if (count($errors) > 0)
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
            <h3><strong> Whoops! </strong>There were some problems with your input. </h3>
            <ul class="list-group">
                @foreach ($errors->all() as $error)
                    <li class="list-group-item list-group-item-danger">{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-haze">
                <i class="fa fa-tasks"></i>
                <span class="caption-subject bold uppercase"> {!! trans('menus.create_permission_group') !!}</span>
            </div>
        </div>
        <div class="portlet-body form">
            <form role="form" action="{{route('admin.access.roles.permission-group.update', $group->id)}}" method="post" class="form-horizontal">
                <input name="_method" type="hidden" value="PATCH">
                <input name="_token" type="hidden" value="{{csrf_token()}}">
                <div class="form-body">
                    <div class="form-group form-md-line-input">
                        <label class="col-md-2 control-label" for="form_control_1">{{trans('validation.attributes.permission_group_name')}}</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" id="form_control_1" name="name" placeholder="{{trans('validation.attributes.permission_group_name')}}" value="{{$group->name}}">
                            <div class="form-control-focus">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-2 col-md-10">
                            <a href="{{route('admin.access.roles.permission-group.index')}}" class="btn default">{{ trans('strings.cancel_button') }}</a>
                            <input type="submit" class="btn blue" value="{{ trans('strings.save_button') }}" />
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop