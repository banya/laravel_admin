@extends ('backend.layouts.main')

@section ('title', trans('menus.role_management') . ' | ' . trans('menus.edit_role'))

@section('page-title')
    <h1>
        {{ trans('menus.user_management') }}
        <small>{{ trans('menus.edit_role') }}</small>
    </h1>
@endsection

@section('css')
    <link rel="stylesheet" href="{{asset('backend/vendor/jstree/dist/themes/default/style.min.css')}}"/>
@stop

@section ('breadcrumbs')
    <li><a href="{!!route('backend.dashboard')!!}"><i class="fa fa-dashboard"></i> {{ trans('menus.dashboard') }}</a><i class="fa fa-angle-right"></i></li>
    <li>{!! link_to_route('admin.access.users.index', trans('menus.user_management')) !!}<i class="fa fa-angle-right"></i></li>
    <li>{!! link_to_route('admin.access.roles_m.index', trans('menus.role_management')) !!}<i class="fa fa-angle-right"></i></li>
    <li class="active">{!! trans('strings.edit') !!}</li>
@stop

@section('content')
    @if (count($errors) > 0)
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
            <h3><strong> Whoops! </strong>There were some problems with your input. </h3>
            <ul class="list-group">
                @foreach ($errors->all() as $error)
                    <li class="list-group-item list-group-item-danger">{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-haze">
                <i class="icon-settings font-green-haze"></i>
                <span class="caption-subject bold uppercase"> {!! trans('menus.edit_role') !!}</span>
            </div>
        </div>
        <div class="portlet-body form">
            <form role="form" action="{{route('admin.access.roles_m.update', $role->id)}}" id = 'edit-role' method="post" class="form-horizontal">
                <input name="_method" type="hidden" value="PATCH">
                <input name="_token" type="hidden" value="{{csrf_token()}}">
                <div class="form-body">
                    <div class="form-group form-md-line-input">
                        <label class="col-md-2 control-label" for="form_control_1">{{trans('validation.attributes.role_name')}}</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" id="form_control_1" name="name" placeholder="{{trans('validation.attributes.role_name')}}" value="{{$role->name}}">
                            <div class="form-control-focus">
                            </div>
                        </div>
                    </div>

                    <div class="form-group form-md-line-input">
                        <label class="col-md-2 control-label" for="form_control_1">{{trans('validation.attributes.associated_permissions')}}</label>
                        <div class="col-md-10">
                            @if ($role->id != 1)
                                <select class="form-control" name="associated-permissions" id="form_control_1">
                                    <option value="all" {{$role->all ? 'selected':''}}>All</option>
                                    <option value="custom" {{$role->all ? '':'selected'}}>Custom</option>
                                </select>
                            @else
                                <span class="label label-success">All</span>
                            @endif
                            <div id="available-permissions" class="hidden">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="alert alert-info">
                                                <i class="fa fa-info-circle"></i> A permission marked with a <small><strong>(D)</strong></small> means that the permission has dependencies. They will be checked automatically when you select that permission. You can manage each permissions dependencies in the dependency tab of the edit permission screen.
                                            </div><!--alert-->
                                        </div><!--col-lg-12-->

                                        <div class="col-lg-6">
                                            <p><strong>Grouped Permissions</strong></p>

                                            @if ($groups->count())
                                                <div id="permission-tree">
                                                    <ul>
                                                        @foreach ($groups as $group)
                                                            <li>{!! $group->name !!}
                                                                @if ($group->permissions->count())
                                                                    <ul>
                                                                        @foreach ($group->permissions as $permission)
                                                                            <li id="{!! $permission->id !!}" data-dependencies="{!! json_encode($permission->dependencies->lists('dependency_id')->all()) !!}">

                                                                                @if ($permission->dependencies->count())
                                                                                    <?php
                                                                                    //Get the dependency list for the tooltip
                                                                                    $dependency_list = [];
                                                                                    foreach ($permission->dependencies as $dependency)
                                                                                        array_push($dependency_list, $dependency->permission->display_name);
                                                                                    $dependency_list = implode(", ", $dependency_list);
                                                                                    ?>
                                                                                    <a data-toggle="tooltip" data-html="true" title="<strong>Dependencies:</strong> {!! $dependency_list !!}">{!! $permission->display_name !!} <small><strong>(D)</strong></small></a>
                                                                                @else
                                                                                    {!! $permission->display_name !!}
                                                                                @endif

                                                                            </li>
                                                                        @endforeach
                                                                    </ul>
                                                                @endif

                                                                @if ($group->children->count())
                                                                    <ul>
                                                                        @foreach ($group->children as $child)
                                                                            <li>{!! $child->name !!}
                                                                                @if ($child->permissions->count())
                                                                                    <ul> style="padding-left:40px;font-size:.8em">
                                                                                        @foreach ($child->permissions as $permission)
                                                                                            <li id="{!! $permission->id !!}" data-dependencies="{!! json_encode($permission->dependencies->lists('dependency_id')->all()) !!}">

                                                                                                @if ($permission->dependencies->count())
                                                                                                    <?php
                                                                                                    //Get the dependency list for the tooltip
                                                                                                    $dependency_list = [];
                                                                                                    foreach ($permission->dependencies as $dependency)
                                                                                                        array_push($dependency_list, $dependency->permission->display_name);
                                                                                                    $dependency_list = implode(", ", $dependency_list);
                                                                                                    ?>
                                                                                                    <a data-toggle="tooltip" data-html="true" title="<strong>Dependencies:</strong> {!! $dependency_list !!}">{!! $permission->display_name !!} <small><strong>(D)</strong></small></a>
                                                                                                @else
                                                                                                    {!! $permission->display_name !!}
                                                                                                @endif

                                                                                            </li>
                                                                                        @endforeach
                                                                                    </ul>
                                                                                @endif
                                                                            </li>
                                                                        @endforeach
                                                                    </ul>
                                                                @endif
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @else
                                                <p>There are no permission groups.</p>
                                            @endif
                                        </div><!--col-lg-6-->

                                        <div class="col-lg-6">
                                            <p><strong>Ungrouped Permissions</strong></p>

                                            @if ($permissions->count())
                                                @foreach ($permissions as $perm)
                                                    <input type="checkbox" name="ungrouped[]" value="{!! $perm->id !!}" id="perm_{!! $perm->id !!}" {{in_array($perm->id, $role_permissions) ? 'checked' : ""}} data-dependencies="{!! json_encode($perm->dependencies->lists('dependency_id')->all()) !!}" /> <label for="perm_{!! $perm->id !!}">

                                                        @if ($perm->dependencies->count())
                                                            <?php
                                                            //Get the dependency list for the tooltip
                                                            $dependency_list = [];
                                                            foreach ($perm->dependencies as $dependency)
                                                                array_push($dependency_list, $dependency->permission->display_name);
                                                            $dependency_list = implode(", ", $dependency_list);
                                                            ?>
                                                            <a style="color:black;text-decoration:none;" data-toggle="tooltip" data-html="true" title="<strong>Dependencies:</strong> {!! $dependency_list !!}">{!! $perm->display_name !!} <small><strong>(D)</strong></small></a>
                                                        @else
                                                            {!! $perm->display_name !!}
                                                        @endif

                                                    </label><br/>
                                                @endforeach
                                            @else
                                                <p>There are no ungrouped permissions.</p>
                                            @endif
                                        </div><!--col-lg-6-->
                                    </div><!--row-->
                                </div><!--available permissions-->
                        </div>
                    </div>

                    <div class="form-group form-md-line-input">
                        <label class="col-md-2 control-label" for="form_control_1">{{trans('validation.attributes.role_sort')}}</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" id="form_control_1" name="sort" placeholder="{{trans('validation.attributes.role_sort')}}" value="{{$role->sort}}">
                            <div class="form-control-focus">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-2 col-md-10">
                            <a href="{{route('admin.access.roles_m.index')}}" class="btn default">{{ trans('strings.cancel_button') }}</a>
                            <input type="submit" class="btn blue" value="{{ trans('strings.save_button') }}" />
                        </div>
                    </div>
                </div>
                <input type="hidden" name="permissions" value="{{$role->permissions}}"/>
            </form>
        </div>
    </div>
@stop

@section('js')
    <script type="text/javascript" src="{{asset('backend/vendor/jstree/dist/jstree.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('backend/js/plugins/roles.js')}}"></script>

    <script>
        $(function() {
            @foreach ($role_permissions as $permission)
                tree.jstree('check_node', '#{!! $permission !!}');
            @endforeach
        });
    </script>
@stop