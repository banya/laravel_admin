@extends ('backend.layouts.main')

@section ('title', trans('menus.user_management') . ' | ' . trans('menus.deleted_users'))

@section('page-title')
    <h1>
        {{ trans('menus.user_management') }}
        <small>{{ trans('menus.deleted_users') }}</small>
    </h1>
@endsection

@section ('breadcrumbs')
    <li><a href="{!!route('backend.dashboard')!!}"><i class="fa fa-dashboard"></i> {{ trans('menus.dashboard') }}</a><i class="fa fa-angle-right"></i></li>
    <li>{!! link_to_route('admin.access.users.index', trans('menus.user_management')) !!}<i class="fa fa-angle-right"></i></li>
    <li class="active">{!! trans('menus.deleted_users') !!}</li>
@stop

@section('content')
    @if(Session::get('flash_success'))
        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
            @if(is_array(json_decode(Session::get('flash_success'),true)))
                {!! implode('', Session::get('flash_success')->all(':message<br/>')) !!}
            @else
                {!! Session::get('flash_success') !!}
            @endif
        </div>
    @endif
    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-th-list"></i>{!! trans('menus.deleted_users') !!}
            </div>
            <div class="tools">
                <a href="javascript:;" class="collapse" data-original-title="" title=""></a>
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-scrollable">
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>{{ trans('crud.users.id') }}</th>
                        <th>{{ trans('crud.users.name') }}</th>
                        <th>{{ trans('crud.users.email') }}</th>
                        <th>{{ trans('crud.users.confirmed') }}</th>
                        <th>{{ trans('crud.users.roles') }}</th>
                        <th>{{ trans('crud.users.other_permissions') }}</th>
                        <th class="visible-lg">{{ trans('crud.users.created') }}</th>
                        <th class="visible-lg">{{ trans('crud.users.last_updated') }}</th>
                        <th>{{ trans('crud.actions') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if ($users->count())
                        @foreach ($users as $user)
                            <tr>
                                <td>{!! $user->id !!}</td>
                                <td>{!! $user->name !!}</td>
                                <td>{!! link_to("mailto:".$user->email, $user->email) !!}</td>
                                <td>{!! $user->confirmed_label !!}</td>
                                <td>
                                    @if ($user->roles()->count() > 0)
                                        @foreach ($user->roles as $role)
                                            {!! $role->name !!}<br/>
                                        @endforeach
                                    @else
                                        None
                                    @endif
                                </td>
                                <td>
                                    @if ($user->permissions()->count() > 0)
                                        <a href="javascript:;" data-id="{{$user->id}}" role="button"  class="btn btn-xs red findOtherPermissions">View</a>
                                    @else
                                        <button class="btn btn-xs btn-default disabled">None</button>
                                    @endif
                                </td>
                                <td class="visible-lg">{!! $user->created_at->diffForHumans() !!}</td>
                                <td class="visible-lg">{!! $user->updated_at->diffForHumans() !!}</td>
                                <td>
                                    @permission('undelete-users')
                                    <a href="{{route('admin.access.user.restore', $user->id)}}" style="color:black;text-decoration:none;" class="btn btn-xs btn-success tooltips" data-container="body" data-original-title="{{trans('crud.users.restore_user')}}" name="restore_user"  data-placement="top"><i class="fa fa-refresh"></i></a>
                                    @endauth
                                    @permission('permanently-delete-users')
                                    <a href="{{route('admin.access.user.delete-permanently',$user->id)}}" data-method="delete" class="btn btn-xs btn-danger tooltips" style="color:black;text-decoration:none" data-container="body" data-original-title="{{trans('crud.delete_button')}}" name="delete_user_perm"  data-placement="top" onclick="$(this).find('form').submit();">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                    @endauth
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <td colspan="9">{{ trans('crud.users.no_deactivated_users') }}</td>
                    @endif
                    </tbody>
                </table>

            </div>
        </div>
        <div class="portlet-title">
            <div class="pull-left">
                <h5 class="block">{!! $users->total() !!} {{ trans('crud.users.total') }}</h5>
            </div>

            <div class="pull-right">
                {!! $users->render() !!}
            </div>
        </div>

    </div>
    <div class="clearfix"></div>
    <div id="myModal2" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Other Permissions</h4>
                </div>
                <div class="modal-body">
                    <p class="per_content">
                        Permissions loading
                    </p>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn green">OK</button>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
    <script src="{{asset('backend/js/plugins/userIndex.js')}}" type="text/javascript"></script>
    <script>
        $(function() {
            @permission('permanently-delete-users')
            $("a[name='delete_user_perm']").click(function() {
                return confirm("Are you sure you want to delete this user permanently? Anywhere in the application that references this user's id will most likely error. Proceed at your own risk. This can not be un-done.");
            });
            @endauth

            @permission('undelete-users')
            $("a[name='restore_user']").click(function() {
                return confirm("Restore this user to its original state?");
            });
            @endauth
		});
    </script>
@stop