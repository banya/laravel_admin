@extends ('backend.layouts.main')

@section ('title', trans('menus.user_management') . ' | ' . trans('menus.create_user'))

@section('page-title')
    <h1>
        {{ trans('menus.user_management') }}
        <small>{{ trans('menus.create_user') }}</small>
    </h1>
@endsection

@section ('breadcrumbs')
    <li><a href="{!!route('backend.dashboard')!!}"><i class="fa fa-dashboard"></i> {{ trans('menus.dashboard') }}</a><i class="fa fa-angle-right"></i></li>
    <li>{!! link_to_route('admin.access.users.index', trans('menus.user_management')) !!}<i class="fa fa-angle-right"></i></li>
    <li class="active">{!! trans('menus.create_user') !!}</li>
@stop

@section('content')
    @if (count($errors) > 0)
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
            <h3><strong> Whoops! </strong>There were some problems with your input. </h3>
            <ul class="list-group">
                @foreach ($errors->all() as $error)
                    <li class="list-group-item list-group-item-danger">{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-haze">
                <i class="icon-settings font-green-haze"></i>
                <span class="caption-subject bold uppercase"> {!! trans('menus.edit_user') !!}</span>
            </div>
            <div class="actions">
                <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title="">
                </a>
            </div>
        </div>
        <div class="portlet-body form">
            <form role="form" action="{{route('admin.access.users.store')}}" method="post" class="form-horizontal">
                <input name="_token" type="hidden" value="{{csrf_token()}}">
                <div class="form-body">
                    <div class="form-group form-md-line-input">
                        <label class="col-md-2 control-label" for="form_control_1">{{trans('validation.attributes.name')}}</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" id="form_control_1" name="name" placeholder="{{trans('strings.full_name')}}">
                            <div class="form-control-focus">
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label class="col-md-2 control-label" for="form_control_1">{{trans('validation.attributes.email')}}</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" id="form_control_1" name="email" placeholder="{{trans('strings.email')}}">
                            <div class="form-control-focus">
                            </div>
                        </div>
                    </div>

                    <div class="form-group form-md-line-input">
                        <label class="col-md-2 control-label" for="form_control_1">{{trans('validation.attributes.password')}}</label>
                        <div class="col-md-10">
                            <input type="password" class="form-control" id="form_control_1" name="password">
                            <div class="form-control-focus">
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label class="col-md-2 control-label" for="form_control_1">{{trans('validation.attributes.password_confirmation')}}</label>
                        <div class="col-md-10">
                            <input type="password" class="form-control" id="form_control_1" name="password_confirmation">
                            <div class="form-control-focus">
                            </div>
                        </div>
                    </div>

                    <div class="form-group form-md-line-input">
                        <label class="col-md-2 control-label" for="form_control_1">{{ trans('validation.attributes.active') }}</label>
                        <div class="col-md-10">
                            <div class="md-checkbox-inline">
                                <div class="md-checkbox has-error">
                                    <input type="checkbox" id="status" value="1" name="status"  class="md-check">
                                    <label for="status">
                                        <span class="inc"></span>
                                        <span class="check"></span>
                                        <span class="box"></span> </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label class="col-md-2 control-label" for="form_control_1">{{ trans('validation.attributes.confirmed') }}</label>
                        <div class="col-md-10">
                            <div class="md-checkbox-inline">
                                <div class="md-checkbox has-error">
                                    <input type="checkbox" id="checkbox34" value="1" name="confirmed" class="md-check">
                                    <label for="checkbox34">
                                        <span class="inc"></span>
                                        <span class="check"></span>
                                        <span class="box"></span> </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label class="col-md-2 control-label" for="form_control_1">{{ trans('validation.attributes.send_confirmation_email') }}<br/>
                            <small>{{ trans('strings.if_confirmed_is_off') }}</small>
                        </label>

                        <div class="col-md-10">
                            <div class="md-checkbox-inline">
                                <div class="md-checkbox has-error">
                                    <input type="checkbox" id="confirmation_email" value="1" name="confirmation_email" class="md-check">
                                    <label for="confirmation_email">
                                        <span class="inc"></span>
                                        <span class="check"></span>
                                        <span class="box"></span> </label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group form-md-line-input">
                        <label class="col-md-2 control-label" for="form_control_1">{{ trans('validation.attributes.associated_roles') }}</label>
                        <div class="col-md-10">
                            @if (count($roles) > 0)
                                <div class="md-checkbox-inline">
                                    @foreach($roles as $role)
                                        <div class="md-checkbox has-error">
                                            <input type="checkbox" id="role-{{$role->id}}" value="{{$role->id}}" name="assignees_roles[]" class="md-check">
                                            <label for="role-{{$role->id}}">
                                                <span class="inc"></span>
                                                <span class="check"></span>
                                                <span class="box"></span>
                                                {!! $role->name !!}
                                                <a href="#role_{{$role->id}}" class="show-permissions small" data-toggle="modal">(<span class="show-hide">Show</span> Permissions)</a>
                                            </label>
                                        </div>
                                        <div id="role_{{$role->id}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true" style="display: none;">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                        <h4 class="modal-title">show permissions</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>
                                                            @if ($role->all)
                                                                All Permissions
                                                            @else
                                                                @if (count($role->permissions) > 0)
                                                        <blockquote class="small">
                                                            @foreach ($role->permissions as $perm)
                                                                {{$perm->display_name}}<br/>
                                                            @endforeach
                                                        </blockquote>
                                                        @else
                                                            No permissions
                                                            @endif
                                                            @endif
                                                            </p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button data-dismiss="modal" class="btn green">OK</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            @else
                                No Roles to set
                            @endif
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                            <label class="col-md-2 control-label" for="form_control_1">{{ trans('validation.attributes.other_permissions') }}</label>
                            <div class="col-md-10">
                                <div class="alert alert-info">
                                    <i class="fa fa-info-circle"></i> Checking a permission will also check its dependencies, if any.
                                </div><!--alert-->
                                @if (count($permissions))
                                    @foreach (array_chunk($permissions->toArray(), 10) as $perm)
                                        <div class="col-lg-4">
                                            <ul style="margin:0;padding:0;list-style:none;">
                                                @foreach ($perm as $p)
                                                    <?php
                                                    //Since we are using array format to nicely display the permissions in rows
                                                    //we will just manually create an array of dependencies since we do not have
                                                    //access to the relationship to use the lists() function of eloquent
                                                    //but the relationships are eager loaded in array format now
                                                    $dependencies = [];
                                                    $dependency_list = [];
                                                    if (count($p['dependencies'])) {
                                                        foreach ($p['dependencies'] as $dependency) {
                                                            array_push($dependencies, $dependency['dependency_id']);
                                                            array_push($dependency_list, $dependency['permission']['display_name']);
                                                        }
                                                    }
                                                    $dependencies = json_encode($dependencies);
                                                    $dependency_list = implode(", ", $dependency_list);
                                                    ?>

                                                    <li>
                                                        <div class="md-checkbox">
                                                            <input type="checkbox" value="{{$p['id']}}" class="md-check" name="permission_user[]" data-dependencies="{!! $dependencies !!}" id="permission-{{$p['id']}}">
                                                            <label for="permission-{{$p['id']}}">
                                                                <span></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>
                                                                @if ($p['dependencies'])
                                                                    <a style="color:black;text-decoration:none;" class="tooltips" data-container="body" data-original-title="<strong>Dependencies:</strong> {!! $dependency_list !!}" data-html="true" data-placement="right">{!! $p['display_name'] !!} <small><strong>(D)</strong></small></a>
                                                                @else
                                                                    {!! $p['display_name'] !!}
                                                                @endif
                                                            </label>
                                                        </div>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endforeach
                                @else
                                    No other permissions
                                @endif
                            </div>
                        </div>
                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-2 col-md-10">
                            <a href="{{route('admin.access.users.index')}}" class="btn default">{{ trans('strings.cancel_button') }}</a>
                            <input type="submit" class="btn blue" value="{{ trans('strings.save_button') }}" />
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop

@section('after-scripts-end')
    {!! HTML::script('js/backend/access/permissions/script.js') !!}
    {!! HTML::script('js/backend/access/users/script.js') !!}
@stop