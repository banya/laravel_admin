<?php namespace App\Models\Access\Permission\Traits\Attribute;

/**
 * Class PermissionGroupAttribute
 * @package App\Models\Access\Permission\Traits\Attribute
 */
trait PermissionGroupAttribute {

    /**
     * @return string
     */
    public function getEditButtonAttribute() {
        if (access()->can('edit-permission-groups'))
            return '<a href="'.route('admin.access.roles.permission-group.edit', $this->id).'" class="btn btn-xs btn-primary tooltips" data-container="body" data-original-title="' . trans('crud.edit_button') . '"  data-placement="top"><i class="fa fa-pencil"></i></a>';
        return '';
    }

    /**
     * @return string
     */
    public function getDeleteButtonAttribute() {
        if (access()->can('delete-permission-groups'))
            return '<a data-method="delete" class="btn btn-xs btn-danger tooltips" data-container="body" data-original-title="'.trans('crud.delete_button').'"  data-placement="top" onclick="$(this).find(\'form\').submit();"><i class="fa fa-times"></i><form action="'.route('admin.access.roles.permission-group.destroy', $this->id).'" method="POST" name="delete_item" style="display:none"><input type="hidden" name="_method" value="delete"><input type="hidden" name="_token" value="'.csrf_token().'"></form></a>';
        return '';
    }

    /**
     * @return string
     */
    public function getActionButtonsAttribute() {
        return $this->getEditButtonAttribute().' '.$this->getDeleteButtonAttribute();
    }
}