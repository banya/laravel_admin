$(function(){
    $('.findOtherPermissions').live('click',function(){
        var id = $(this).attr('data-id');
        var model = $('#myModal2');
        var content = $('.per_content');
        $.ajax({
            url:'/admin/access/users/findOtherPermissions/'+id,
            dataType:'html',
            beforeSend:function(){
                Metronic.startPageLoading({message: 'Please wait...'});
            },
            success:function(data){
                content.html(data);
                model.modal();
                Metronic.stopPageLoading();
            }
        });
    });
});